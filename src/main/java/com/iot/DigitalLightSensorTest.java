package com.iot;

/**
 * Created by Farhan Masood on 4/11/2017.
 */

import com.pi4j.io.gpio.*;
import org.json.JSONObject;
import java.io.IOException;

public class DigitalLightSensorTest extends Test implements Runnable {

    private static String postUrl = "https://thingspace.io/dweet/for/"+ getDeviceName() +"_digitalLightSensor";

    public static void main(String... args) {
        test();
    }

    public static void test(){

        final GpioPinDigitalInput gpio16 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_27, PinPullResistance.PULL_UP);

        gpio16.setShutdownOptions(true);

        while(true) {

            PinState pinState = gpio16.getState();

            String request = getRequest(pinState.getValue());

            try {
                System.out.println("DigitalLightSensor Request");
                System.out.println(postUrl);
                System.out.println(request);
                System.out.println();

                post(postUrl, request);

            } catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    private static String getRequest(int value){

        JSONObject jsonObject = new JSONObject();

        jsonObject.put("light", String.valueOf(value));
        jsonObject.put("date", getDate());
        jsonObject.put("time", getTime());

        return jsonObject.toString();
    }

    @Override
    public void run(){
        test();
    }
}
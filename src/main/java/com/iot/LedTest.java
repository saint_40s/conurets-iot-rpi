package com.iot;

import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import org.json.JSONObject;
import java.util.Random;

public class LedTest extends Test implements Runnable {

    private static String postUrl = "https://thingspace.io/dweet/for/"+ getDeviceName() +"_led";

    //17 in Rpi
    private static final GpioPinDigitalOutput gpio17_blue;

    //27 in Rpi
    private static final GpioPinDigitalOutput gpio27_green;

    //22 in Rpi
    private static final GpioPinDigitalOutput gpio22_red;

    //25 in Rpi
    private static final GpioPinDigitalOutput gpio25_yellow;

    private static boolean blueOn = false;
    private static boolean greenOn = false;
    private static boolean redOn = false;
    private static boolean yellowOn = false;

    static {

        gpio17_blue = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00, PinState.LOW);
        gpio27_green = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02, PinState.LOW);
        gpio22_red = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_03, PinState.LOW);
        gpio25_yellow = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_06, PinState.LOW);

        gpio17_blue.setShutdownOptions(true, PinState.LOW);
        gpio27_green.setShutdownOptions(true, PinState.LOW);
        gpio22_red.setShutdownOptions(true, PinState.LOW);
        gpio25_yellow.setShutdownOptions(true, PinState.LOW);
    }

    private static String getRequest() throws Exception {

        Random blueRandomGenerator = new Random();
        Random greenRandomGenerator = new Random();
        Random redRandomGenerator = new Random();
        Random yellowRandomGenerator = new Random();
        JSONObject jsonObject = new JSONObject();

        int blueRandomNumber = blueRandomGenerator.nextInt(16001);
        int greenRandomNumber = greenRandomGenerator.nextInt(16001);
        int redRandomNumber = redRandomGenerator.nextInt(16001);
        int yellowRandomNumber = yellowRandomGenerator.nextInt(16001);

        jsonObject.put("blue", "0");
        jsonObject.put("green", "0");
        jsonObject.put("red", "0");
        jsonObject.put("yellow", "0");

        jsonObject.put("date", getDate());
        jsonObject.put("time", getTime());
//        jsonObject.put("text", "Request No. " + count);

        blueOn = false;
        greenOn = false;
        redOn = false;
        yellowOn = false;

        if(blueRandomNumber >= 0 && blueRandomNumber <= 4000){

            blueOn = true;
            jsonObject.put("blue", "1");
        }

        if(greenRandomNumber >= 4001 && greenRandomNumber <= 8000){

            greenOn = true;
            jsonObject.put("green", "1");
        }

        if(redRandomNumber >= 8001 && redRandomNumber <= 12000){

            redOn = true;
            jsonObject.put("red", "1");
        }

        if(yellowRandomNumber >= 12001 && yellowRandomNumber <= 16000){

            yellowOn = true;
            jsonObject.put("yellow", "1");
        }

        return jsonObject.toString();
    }

    public static void main(String... args) {
        test();
    }

    public static void test() {

        try {
            boolean indicator = true;

            while (indicator) {

                String request = getRequest();

                System.out.println("LED Sensor Request");
                System.out.println(postUrl);
                System.out.println(request);
                System.out.println();

                String response = post(postUrl, request);

                updateLedsOnRaspPi(blueOn, greenOn, redOn, yellowOn);

                delay(5000);
            }

        } catch(Exception e){
            e.printStackTrace();
        }
    }

    private static void updateLedsOnRaspPi(boolean blueOn, boolean greenOn, boolean redOn, boolean yellowOn)
            throws Exception {

        if(blueOn){
            gpio17_blue.high();

        } else {
            gpio17_blue.low();
        }

        if(greenOn) {
            gpio27_green.high();

        } else {
            gpio27_green.low();
        }

        if(redOn) {
            gpio22_red.high();

        } else {
            gpio22_red.low();
        }

        if(yellowOn) {
            gpio25_yellow.high();

        } else {
            gpio25_yellow.low();
        }
    }

    @Override
    public void run(){
        test();
    }
}
package com.iot;

import com.iot.util.PropertiesUtil;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import okhttp3.*;

import java.io.IOException;
import java.time.LocalTime;
import java.time.LocalDate;

/**
 * Created by Farhan Masood on 4/12/2017.
 */
public class Test {

    protected static final GpioController gpio;
    private static OkHttpClient client;

    private static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    static {
        gpio = GpioFactory.getInstance();
        client = new OkHttpClient();
    }

    protected static String post(String url, String json) throws IOException {

        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    protected static void delay(int delayNumber){

        try {
            Thread.sleep(delayNumber);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected static String getTime(){

        LocalTime localTime = LocalTime.now();
        return localTime.getHour() + ":" + localTime.getMinute() + ":" + localTime.getSecond();
    }

    protected static String getDeviceName(){

        return PropertiesUtil.getProperty("device.name");
    }

    protected static String getDate(){

        LocalDate localDate = LocalDate.now();
        return localDate.getMonthValue() + "/" + localDate.getDayOfMonth() + "/" + localDate.getYear();
    }
}

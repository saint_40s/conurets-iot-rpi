package com.iot;

/**
 * Created by Farhan Masood on 4/11/2017.
 */
import com.iot.util.PropertiesUtil;

public class Development {

    public static void main(String args[]) throws InterruptedException {

        new Thread(new LedTest()).start();
        new Thread(new DigitalLightSensorTest()).start();
        new Thread(new TemperatureAndHumiditySensorTest()).start();
    }
}
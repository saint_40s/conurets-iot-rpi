package com.iot.util;

import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Farhan Masood on 4/18/2017.
 */
public class PropertiesUtil {

    private static Properties properties = null;

    static {

        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream inputStream = classloader.getResourceAsStream("iot.properties");

            properties = new Properties();
            properties.load(inputStream);

        } catch(Exception e){
            e.printStackTrace();
        }
    }

    public static String getProperty(String property){
        return properties.getProperty(property);
    }
}
